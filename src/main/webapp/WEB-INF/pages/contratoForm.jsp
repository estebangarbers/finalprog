<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Contratos | ${usuario.nombre} | Ministerio del Interior</title>
<link rel='shortcut icon' type='image/x-icon' href='${pageContext.request.contextPath}/resources/img/favicon.ico' />
<link href="${pageContext.request.contextPath}/resources/css/site.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
<script src="${pageContext.request.contextPath}/resources/js/site.min.js"></script>
</head>
<body>
    <jsp:include page="header.jsp"/>
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <h3>Contratos (Nuevo / Editar)</h3>
                        </div>
                        <div class="col-md-6">
                            <a href="${pageContext.request.contextPath}/vigilante/contratos?id=${usuario.id}" style="margin-top: 15px;" class="btn btn-default pull-right">Volver</a>
                        </div>
                    </div>                
                <hr>
		
                <div class="row">
                    <form:form action="${pageContext.request.contextPath}/vigilante/contratos/save" method="post" modelAttribute="contrato">
                        <form:hidden path="id"/>
                        <form:hidden path="usuarioId" value="${usuario.id}"/>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Sucursal</label>
                                <form:select path="sucursal" class="form-control" required="required">
                                    <form:options items="${listSucursales}" itemValue="id" itemLabel="domicilio"></form:options>
                                </form:select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Fecha de contratación:</label>
                                <form:input type="date" path="fecha" class="form-control" required="required" />
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Armado:</label>
                                <form:select path="armado" class="form-control" required="required">
                                    <form:option value="1">Si</form:option>
                                    <form:option value="0">No</form:option>
                                </form:select>
                            </div>
                        </div>
                            
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary">Continuar</button>
                        </div>
                    </form:form>
                </div>
	</div>
</body>
</html>