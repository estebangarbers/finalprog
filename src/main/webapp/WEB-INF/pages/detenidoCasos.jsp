<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Casos | ${detenido.nombre} | Ministerio del Interior</title>
<link rel='shortcut icon' type='image/x-icon' href='${pageContext.request.contextPath}/resources/img/favicon.ico' />
<link href="${pageContext.request.contextPath}/resources/css/site.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
<script src="${pageContext.request.contextPath}/resources/js/site.min.js"></script>
</head>
<body>
    <jsp:include page="header.jsp"/>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h3>Detenido ${detenido.nombre}</h3>
            </div>
            <div class="col-md-6">
                <a href="${pageContext.request.contextPath}/detenidos" style="margin-top: 15px;margin-right:10px;" class="btn btn-default pull-right">Volver</a>
            </div>
        </div>                
        <hr>
        ${msg}
        <c:choose>
        <c:when test="${empty listCasos}">
            <div class="alert alert-warning">El detenido no posee casos registrados.</div>
        </c:when>
        <c:otherwise>
            <table class="table table-bordered">
                <th>ID</th>
                <th>Fecha</th>
                <th>Sucursal</th>
                <th>Juez a cargo</th>
                <th>Condena</th>
                <c:forEach var="caso" items="${listCasos}">
                <tr>
                    <td>${caso.id}</td>
                    <td>${caso.fecha}</td>
                    <td>${caso.sucursal.id} - ${caso.sucursal.domicilio} <br><span class="text-muted" style="font-size:0.8em;"><b>Entidad: </b> ${caso.sucursal.entidad.domicilio}</span></td>
                    <td>${caso.juez.nombre}</td>
                    <td>
                        <c:if test = "${caso.condena == 0}">
                            Sin condena
                        </c:if>
                        <c:if test = "${caso.condena > 0}">
                            ${caso.condena} años
                        </c:if>
                    </td>
                </tr>
                </c:forEach>
            </table>
        </c:otherwise>
        </c:choose>
    </div>
</body>
</html>