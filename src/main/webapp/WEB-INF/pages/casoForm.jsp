<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Casos | Ministerio del Interior</title>
<link rel='shortcut icon' type='image/x-icon' href='${pageContext.request.contextPath}/resources/img/favicon.ico' />
<link href="${pageContext.request.contextPath}/resources/css/site.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
<script src="${pageContext.request.contextPath}/resources/js/site.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/scripts.js"></script>
</head>
<body>
    <jsp:include page="header.jsp"/>
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <h3>Casos (Nuevo / Editar)</h3>
                        </div>
                        <div class="col-md-6">
                            <a href="${pageContext.request.contextPath}/casos" style="margin-top: 15px;" class="btn btn-default pull-right">Volver</a>
                        </div>
                    </div>                
                <hr>
		
                <div class="row">
                    <form:form action="${pageContext.request.contextPath}/casos/save" method="post" modelAttribute="caso">
                        <form:hidden path="id"/>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Detenido involucrado:</label>
                                <form:select path="detenido" class="form-control" required="required">
                                    <form:options items="${listDetenidos}" itemValue="id" itemLabel="nombre"></form:options>
                                </form:select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Sucursal del hecho:</label>
                                <form:select path="sucursal" class="form-control" required="required">
                                    <form:options items="${listSucursales}" itemValue="id" itemLabel="domicilio"></form:options>
                                </form:select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Juez a cargo:</label>
                                <form:select path="juez" class="form-control" required="required">
                                    <form:options items="${listJueces}" itemValue="id" itemLabel="nombre"></form:options>
                                </form:select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Fecha del ilícito:</label>
                                <form:input type="date" path="fecha" class="form-control" required="required" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                
                                <label>¿Hubo condena?</label>
                                <select id="hubocondena" name="hubocondena" class="form-control" required="required">
                                    <option value="0">No</option>
                                    <option value="1" <c:if test = "${caso.condena > 0}">selected</c:if>>Si</option>
                                </select>
                            </div>
                        </div>   
                        <div class="col-md-6" id="condenaCont">
                            <div class="form-group">
                                <label>Condena:</label>
                                <form:input type="text" path="condena" id="condena" class="form-control" required="required" />
                            </div>
                        </div>                            
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary">Continuar</button>
                        </div>
                    </form:form>
                </div>
	</div>
</body>
</html>