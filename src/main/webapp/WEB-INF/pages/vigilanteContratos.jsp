<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Contratos | ${usuario.nombre} | Ministerio del Interior</title>
<link rel='shortcut icon' type='image/x-icon' href='${pageContext.request.contextPath}/resources/img/favicon.ico' />
<link href="${pageContext.request.contextPath}/resources/css/site.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
<script src="${pageContext.request.contextPath}/resources/js/site.min.js"></script>
</head>
<body>
    <jsp:include page="header.jsp"/>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h3>Vigilante: ${usuario.nombre}</h3>
            </div>
            <div class="col-md-6">
                <c:if test="${sessionScope.userRol.equals('Administrador')}">   
                <a href="contratos/nuevo?id=${usuario.id}" style="margin-top: 15px;" class="btn btn-success pull-right">Nuevo contrato</a>
                </c:if>
                <a href="${pageContext.request.contextPath}/vigilantes" style="margin-top: 15px;margin-right:10px;" class="btn btn-default pull-right">Volver</a>
            </div>
        </div>                
        <hr>
        ${msg}
        <c:choose>
        <c:when test="${empty contratos}">
            <div class="alert alert-warning">El vigilante no posee contratos registrados.</div>
        </c:when>
        <c:otherwise>
            <table class="table table-bordered">
                <th>Sucursal</th>
                <th>Fecha</th>
                <th>Armado</th>
                <c:if test="${sessionScope.userRol.equals('Administrador')}">   
                <th width="100px"></th>
                </c:if>
                <c:forEach var="contrato" items="${contratos}">
                <tr>
                    <td>${contrato.sucursal.id} - ${contrato.sucursal.domicilio}</td>
                    <td>${contrato.fecha}</td>
                    <td>
                        <c:if test = "${contrato.armado == 1}">
                            Si
                        </c:if>
                        <c:if test = "${contrato.armado == 0}">
                            No
                        </c:if>
                    </td>
                    <c:if test="${sessionScope.userRol.equals('Administrador')}">   
                    <td class="text-center">
                        <div class="btn-group">
                        <a href="contratos/editar?id=${contrato.id}" class="btn btn-warning"><i class="fa fa-edit"></i></a>
                        <a href="contratos/eliminar?id=${contrato.id}&idVigilante=${usuario.id}" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                        </div>
                    </td>
                    </c:if>
                </tr>
                </c:forEach>
            </table>
        </c:otherwise>
        </c:choose>
    </div>
</body>
</html>