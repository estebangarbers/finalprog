<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Contratos | ${usuario.nombre} | Ministerio del Interior</title>
<link rel='shortcut icon' type='image/x-icon' href='${pageContext.request.contextPath}/resources/img/favicon.ico' />
<link href="${pageContext.request.contextPath}/resources/css/site.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
<script src="${pageContext.request.contextPath}/resources/js/site.min.js"></script>
</head>
<body>
    <jsp:include page="headerVigilante.jsp"/>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3>Bienvenido ${usuario.nombre}</h3>
            </div>
            <div class="col-md-12">
                <p>A continuación se detallan los contratos que se le han asignado:</p>
            </div>
        </div>                
        <hr>
        ${msg}
        <table class="table table-bordered">
            <th>Sucursal</th>
            <th>Fecha</th>
            <th>Armado</th>
            <c:forEach var="contrato" items="${contratos}">
            <tr>
                <td>${contrato.sucursal.id} - ${contrato.sucursal.domicilio}</td>
                <td>${contrato.fecha}</td>
                <td>
                    <c:if test = "${contrato.armado == 1}">
                        Si
                    </c:if>
                    <c:if test = "${contrato.armado == 0}">
                        No
                    </c:if>
                </td>
            </tr>
            </c:forEach>
        </table>
    </div>
</body>
</html>