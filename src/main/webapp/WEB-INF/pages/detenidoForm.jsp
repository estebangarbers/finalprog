<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Detenidos | Ministerio del Interior</title>
<link rel='shortcut icon' type='image/x-icon' href='${pageContext.request.contextPath}/resources/img/favicon.ico' />
<link href="${pageContext.request.contextPath}/resources/css/site.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
<script src="${pageContext.request.contextPath}/resources/js/site.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/scripts.js"></script>
</head>
<body>
    <jsp:include page="header.jsp"/>
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <h3>Detenido (Nuevo / Editar)</h3>
                        </div>
                        <div class="col-md-6">
                            <a href="${pageContext.request.contextPath}/detenidos" style="margin-top: 15px;" class="btn btn-default pull-right">Volver</a>
                        </div>
                    </div>                
                <hr>
		
                <div class="row">
                    <form:form action="${pageContext.request.contextPath}/detenidos/save" method="post" modelAttribute="detenido">
                        <form:hidden path="id"/>
                        <div class="col-md-4">
                            <div class="form-group">
                                <form:label path="nombre">Nombre:</form:label>
                                <form:input path="nombre" class="form-control" required="required" />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <form:label path="banda">Banda:</form:label>
                                <form:select path="banda" id="banda" class="form-control" required="required">
                                    <form:option value="0">Registrar nueva</form:option>
                                    <form:options items="${listBandas}" itemValue="banda" itemLabel="banda"></form:options>
                                </form:select>
                            </div>
                        </div>
                        <div class="col-md-4" id="nombreBandaCont">
                            <div class="form-group">
                                <label>Nombre de la banda:</label>                                
                                <input type="text" name="nombreBanda" id="nombreBanda" class="form-control" required="required" />
                            </div>
                        </div>
                            
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary">Continuar</button>
                        </div>
                    </form:form>
                </div>
	</div>
</body>
</html>