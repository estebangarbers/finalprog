<style>
    .table tr td{
        vertical-align:middle!important;
    }
</style>
<nav class="navbar navbar-inverse" role="navigation" style="border-radius:0;">
    <div class="container-fluid">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-5">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="${pageContext.request.contextPath}/home">Ministerio del Interior</a>
      </div>
      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-5">
          <p class="navbar-text navbar-right"><a class="navbar-link" href="${pageContext.request.contextPath}/logout">Sesi�n iniciada: <%= session.getAttribute("userName") %> | Cerrar sesi�n</a></p>
      </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>