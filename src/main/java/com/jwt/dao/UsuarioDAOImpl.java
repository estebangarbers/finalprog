package com.jwt.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jwt.model.Usuario;
import org.hibernate.Query;

@Repository
public class UsuarioDAOImpl implements UsuarioDAO {

	@Autowired
	private SessionFactory sessionFactory;

	public void addUsuario(Usuario usuario) {
		sessionFactory.getCurrentSession().saveOrUpdate(usuario);

	}

	@SuppressWarnings("unchecked")
	public List<Usuario> getAllUsuarios() {

		return sessionFactory.getCurrentSession().createQuery("from Usuario")
				.list();
	}

	@Override
	public void deleteUsuario(Integer usuarioId) {
		Usuario usuario = (Usuario) sessionFactory.getCurrentSession().load(
				Usuario.class, usuarioId);
		if (null != usuario) {
			this.sessionFactory.getCurrentSession().delete(usuario);
		}

	}

	public Usuario getUsuario(int empid) {
		return (Usuario) sessionFactory.getCurrentSession().get(
				Usuario.class, empid);
	}
        
        public List<Usuario> getUsuarioByUserAndPassword(String user, String password) {
                
		Query q = sessionFactory.getCurrentSession().createQuery("from Usuario WHERE usuario = :user AND password = :password");
                q.setParameter("user",user);  
                q.setParameter("password",password);  
                List<Usuario> usuario=q.list();  
                return usuario;
	}
        
        public boolean getUsuarioByUser(String user) {
                
		Query q = sessionFactory.getCurrentSession().createQuery("from Usuario WHERE usuario = :user");
                q.setParameter("user",user);
                List<Usuario> usuario=q.list();  
                return usuario.isEmpty();
	}
        
        public List<Usuario> getAllUsuariosByRol(String rol) {                
            Query q = sessionFactory.getCurrentSession().createQuery("from Usuario WHERE rol = :rol");
            q.setParameter("rol",rol); 
            List<Usuario> usuario=q.list();  
            return usuario;
	}

	@Override
	public Usuario updateUsuario(Usuario usuario) {
		sessionFactory.getCurrentSession().update(usuario);
		return usuario;
	}

}