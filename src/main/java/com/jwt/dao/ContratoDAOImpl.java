package com.jwt.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jwt.model.Contrato;
import org.hibernate.Query;
import org.hibernate.criterion.CriteriaSpecification;

@Repository
public class ContratoDAOImpl implements ContratoDAO {

	@Autowired
	private SessionFactory sessionFactory;

	public void addContrato(Contrato contrato) {
		sessionFactory.getCurrentSession().saveOrUpdate(contrato);

	}

	@SuppressWarnings("unchecked")
	public List<Contrato> getAllContratos() {

		return sessionFactory.getCurrentSession().createQuery("from Contrato")
				.list();
	}

	@Override
	public void deleteContrato(Integer contratoId) {
		Contrato contrato = (Contrato) sessionFactory.getCurrentSession().load(
				Contrato.class, contratoId);
		if (null != contrato) {
			this.sessionFactory.getCurrentSession().delete(contrato);
		}

	}

        @Override
	public Contrato getContrato(int empid) {
		return (Contrato) sessionFactory.getCurrentSession().get(
				Contrato.class, empid);
	}
        
        @Override
        public List<Contrato> getAllContratosByVigilante(int vigilante) {                
            Query q = sessionFactory.getCurrentSession().createQuery("from Contrato WHERE usuario_id = :vigilante");
            q.setParameter("vigilante",vigilante); 
            List<Contrato> contrato=q.list();  
            return contrato;
	}
        
        @Override
        public int getAllContratosBySucursal(int sucursal) {                
            Query q = sessionFactory.getCurrentSession().createQuery("from Contrato WHERE sucursal_id = :sucursal");
            q.setParameter("sucursal",sucursal); 
            q.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
            List<Contrato> contrato=q.list();  
            return contrato.size();
	}
        
        @Override
        public boolean checkContrato(String fecha, int vigilante) {                
            Query q = sessionFactory.getCurrentSession().createQuery("from Contrato WHERE usuario_id = :vigilante AND fecha = :fecha");
            q.setParameter("fecha",fecha);
            q.setParameter("vigilante",vigilante);
            List<Contrato> contrato=q.list(); 
            
            return contrato.isEmpty();
	}

	@Override
	public Contrato updateContrato(Contrato contrato) {
		sessionFactory.getCurrentSession().update(contrato);
		return contrato;
	}

}