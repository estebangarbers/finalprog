package com.jwt.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jwt.model.Juez;
import org.hibernate.Query;

@Repository
public class JuezDAOImpl implements JuezDAO {

	@Autowired
	private SessionFactory sessionFactory;

	public void addJuez(Juez juez) {
		sessionFactory.getCurrentSession().saveOrUpdate(juez);

	}

	@SuppressWarnings("unchecked")
	public List<Juez> getAllJueces() {

		return sessionFactory.getCurrentSession().createQuery("from Juez")
				.list();
	}

	@Override
	public void deleteJuez(Integer juezId) {
		Juez juez = (Juez) sessionFactory.getCurrentSession().load(
				Juez.class, juezId);
		if (null != juez) {
			this.sessionFactory.getCurrentSession().delete(juez);
		}

	}

	public Juez getJuez(int empid) {
		return (Juez) sessionFactory.getCurrentSession().get(
				Juez.class, empid);
	}

	@Override
	public Juez updateJuez(Juez juez) {
		sessionFactory.getCurrentSession().update(juez);
		return juez;
	}
}