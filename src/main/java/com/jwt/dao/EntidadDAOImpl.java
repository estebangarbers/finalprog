package com.jwt.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jwt.model.Entidad;
import org.hibernate.Query;

@Repository
public class EntidadDAOImpl implements EntidadDAO {

	@Autowired
	private SessionFactory sessionFactory;

	public void addEntidad(Entidad entidad) {
		sessionFactory.getCurrentSession().saveOrUpdate(entidad);

	}

	@SuppressWarnings("unchecked")
	public List<Entidad> getAllEntidades() {
		return sessionFactory.getCurrentSession().createQuery("from Entidad")
				.list();
	}

	@Override
	public void deleteEntidad(Integer entidadId) {
		Entidad entidad = (Entidad) sessionFactory.getCurrentSession().load(
				Entidad.class, entidadId);
		if (null != entidad) {
			this.sessionFactory.getCurrentSession().delete(entidad);
		}

	}

	public Entidad getEntidad(int empid) {
		return (Entidad) sessionFactory.getCurrentSession().get(
				Entidad.class, empid);
	}
        
        @Override
        public boolean checkEntidadByDomicilio(String domicilio) {
            Query q = sessionFactory.getCurrentSession().createQuery("from Entidad WHERE domicilio = :domicilio");
            q.setParameter("domicilio",domicilio);
            List<Entidad> entidad=q.list();
            return entidad.isEmpty();
	}

	@Override
	public Entidad updateEntidad(Entidad entidad) {
		sessionFactory.getCurrentSession().update(entidad);
		return entidad;
	}

}