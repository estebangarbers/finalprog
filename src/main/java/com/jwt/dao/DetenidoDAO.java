package com.jwt.dao;

import java.util.List;
import com.jwt.model.Detenido;

public interface DetenidoDAO {

	public void addDetenido(Detenido detenido);

	public List<Detenido> getAllDetenidos();
        
        public List<Detenido> getAllBandas();

	public void deleteDetenido(Integer detenidoId);
        
        public int countMiembros(String banda);

	public Detenido updateDetenido(Detenido detenido);

	public Detenido getDetenido(int detenidoid);
      
}
