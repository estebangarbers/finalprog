package com.jwt.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jwt.model.Sucursal;
import org.hibernate.Query;
import static sun.security.krb5.Confounder.intValue;

@Repository
public class SucursalDAOImpl implements SucursalDAO {

	@Autowired
	private SessionFactory sessionFactory;

        @Override
	public void addSucursal(Sucursal sucursal) {
		sessionFactory.getCurrentSession().saveOrUpdate(sucursal);

	}

        @Override
	@SuppressWarnings("unchecked")
	public List<Sucursal> getAllSucursales() {
		return sessionFactory.getCurrentSession().createQuery("from Sucursal")
				.list();
	}

	@Override
	public void deleteSucursal(Integer sucursalId) {
		Sucursal sucursal = (Sucursal) sessionFactory.getCurrentSession().load(
				Sucursal.class, sucursalId);
		if (null != sucursal) {
			this.sessionFactory.getCurrentSession().delete(sucursal);
		}

	}
        
	public Long countEmpleados(int sucursalId) {
		Query q = sessionFactory.getCurrentSession().createQuery("select count(*) from Contrato where sucursal_id = :sucursal_id");
                q.setParameter("sucursal_id",sucursalId);
                Long resultado;
                resultado = (Long)q.uniqueResult();
                return resultado;
        }
        
        @Override
	public Sucursal getSucursal(int empid) {
		return (Sucursal) sessionFactory.getCurrentSession().get(
				Sucursal.class, empid);
	}
        
        @Override
        public boolean checkSucursalByDomicilio(String domicilio) {
            Query q = sessionFactory.getCurrentSession().createQuery("from Sucursal WHERE domicilio = :domicilio");
            q.setParameter("domicilio",domicilio);
            List<Sucursal> sucursal=q.list();
            return sucursal.isEmpty();
	}

	@Override
	public Sucursal updateSucursal(Sucursal sucursal) {
		sessionFactory.getCurrentSession().update(sucursal);
		return sucursal;
	}

}