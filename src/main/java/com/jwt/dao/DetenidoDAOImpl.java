package com.jwt.dao;

import com.jwt.model.Contrato;
import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jwt.model.Detenido;
import org.hibernate.Query;
import org.hibernate.criterion.CriteriaSpecification;

@Repository
public class DetenidoDAOImpl implements DetenidoDAO {

	@Autowired
	private SessionFactory sessionFactory;

        @Override
	public void addDetenido(Detenido detenido) {
		sessionFactory.getCurrentSession().saveOrUpdate(detenido);

	}

        @Override
	@SuppressWarnings("unchecked")
	public List<Detenido> getAllDetenidos() {
		return sessionFactory.getCurrentSession().createQuery("from Detenido")
				.list();
	}
        
        @Override
	@SuppressWarnings("unchecked")
	public List<Detenido> getAllBandas() {
		return sessionFactory.getCurrentSession().createQuery("from Detenido GROUP BY banda")
				.list();
	}
        
        @Override
        public int countMiembros(String banda){
            Query q = sessionFactory.getCurrentSession().createQuery("from Detenido WHERE banda = :banda");
            q.setParameter("banda",banda); 
            q.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
            List<Detenido> detenido=q.list();  
            return detenido.size();
        }

	@Override
	public void deleteDetenido(Integer detenidoId) {
		Detenido detenido = (Detenido) sessionFactory.getCurrentSession().load(
				Detenido.class, detenidoId);
		if (null != detenido) {
			this.sessionFactory.getCurrentSession().delete(detenido);
		}
	}
        
        @Override
	public Detenido getDetenido(int empid) {
		return (Detenido) sessionFactory.getCurrentSession().get(
				Detenido.class, empid);
	}

	@Override
	public Detenido updateDetenido(Detenido detenido) {
		sessionFactory.getCurrentSession().update(detenido);
		return detenido;
	}

}