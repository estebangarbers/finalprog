package com.jwt.dao;

import java.util.List;
import com.jwt.model.Juez;

public interface JuezDAO {

	public void addJuez(Juez juez);

	public List<Juez> getAllJueces();

	public void deleteJuez(Integer juezId);

	public Juez updateJuez(Juez juez);

	public Juez getJuez(int juezid);
}
