package com.jwt.dao;

import java.util.List;
import com.jwt.model.Entidad;

public interface EntidadDAO {

	public void addEntidad(Entidad entidad);

	public List<Entidad> getAllEntidades();

	public void deleteEntidad(Integer entidadId);

	public Entidad updateEntidad(Entidad entidad);

	public Entidad getEntidad(int entidadid);
        
        public boolean checkEntidadByDomicilio(String domicilio);
      
}
