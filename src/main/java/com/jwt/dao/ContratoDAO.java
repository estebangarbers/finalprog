package com.jwt.dao;

import java.util.List;
import com.jwt.model.Contrato;

public interface ContratoDAO {

	public void addContrato(Contrato contrato);

	public List<Contrato> getAllContratos();

	public void deleteContrato(Integer contratoId);

	public Contrato updateContrato(Contrato contrato);

	public Contrato getContrato(int contratoid);
        
        public List<Contrato> getAllContratosByVigilante(int vigilante);
        
        public int getAllContratosBySucursal(int sucursal);
        
        public boolean checkContrato(String fecha, int vigilante);
}
