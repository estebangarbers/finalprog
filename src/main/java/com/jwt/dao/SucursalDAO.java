package com.jwt.dao;

import java.util.List;
import com.jwt.model.Sucursal;

public interface SucursalDAO {

	public void addSucursal(Sucursal sucursal);

	public List<Sucursal> getAllSucursales();

	public void deleteSucursal(Integer sucursalId);

	public Sucursal updateSucursal(Sucursal sucursal);

	public Sucursal getSucursal(int sucursalid);
        
        public Long countEmpleados(int sucursalid);
        
        public boolean checkSucursalByDomicilio(String domicilio);
      
}
