package com.jwt.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jwt.model.Caso;
import org.hibernate.Query;

@Repository
public class CasoDAOImpl implements CasoDAO {

	@Autowired
	private SessionFactory sessionFactory;

        @Override
	public void addCaso(Caso caso) {
		sessionFactory.getCurrentSession().saveOrUpdate(caso);
	}

        @Override
	@SuppressWarnings("unchecked")
	public List<Caso> getAllCasos() {
		return sessionFactory.getCurrentSession().createQuery("from Caso")
				.list();
	}

	@Override
	public void deleteCaso(Integer casoId) {
		Caso caso = (Caso) sessionFactory.getCurrentSession().load(
				Caso.class, casoId);
		if (null != caso) {
			this.sessionFactory.getCurrentSession().delete(caso);
		}

	}

        @Override
	public Caso getCaso(Integer casoId) {
		return (Caso) sessionFactory.getCurrentSession().get(
				Caso.class, casoId);
	}
        
        @Override
        public boolean checkCaso(String fecha, int detenido) {                
            Query q = sessionFactory.getCurrentSession().createQuery("from Caso WHERE detenido_id = :detenido AND fecha = :fecha");
            q.setParameter("fecha",fecha);
            q.setParameter("detenido",detenido);
            List<Caso> caso=q.list();
            
            return caso.isEmpty();
	}

	@Override
	public Caso updateCaso(Caso caso) {
		sessionFactory.getCurrentSession().update(caso);
		return caso;
	}
        
        @Override
        public List<Caso> getAllCasosByDetenido(int detenido) {                
            Query q = sessionFactory.getCurrentSession().createQuery("from Caso WHERE detenido_id = :detenido");
            q.setParameter("detenido",detenido); 
            List<Caso> casos=q.list();  
            return casos;
	}

}