package com.jwt.dao;

import java.util.List;
import com.jwt.model.Caso;

public interface CasoDAO {

	public void addCaso(Caso caso);

	public List<Caso> getAllCasos();

	public void deleteCaso(Integer casoId);

	public Caso updateCaso(Caso caso);

	public Caso getCaso(Integer casoId);
        
        public boolean checkCaso(String fecha, int detenido);
        
        public List<Caso> getAllCasosByDetenido(int detenido);
}