package com.jwt.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jwt.dao.EntidadDAO;
import com.jwt.model.Entidad;

@Service
@Transactional
public class EntidadServiceImpl implements EntidadService {

	@Autowired
	private EntidadDAO entidadDAO;

	@Override
	@Transactional
	public void addEntidad(Entidad entidad) {
		entidadDAO.addEntidad(entidad);
	}

	@Override
	@Transactional
	public List<Entidad> getAllEntidades() {
		return entidadDAO.getAllEntidades();
	}

	@Override
	@Transactional
	public void deleteEntidad(Integer entidadId) {
		entidadDAO.deleteEntidad(entidadId);
	}

	public Entidad getEntidad(int entidadid) {
		return entidadDAO.getEntidad(entidadid);
	}
        
        @Override
        public boolean checkEntidadByDomicilio(String domicilio) {
                return entidadDAO.checkEntidadByDomicilio(domicilio);
        }

	public Entidad updateEntidad(Entidad entidad) {
		// TODO Auto-generated method stub
		return entidadDAO.updateEntidad(entidad);
	}

	public void setEntidadDAO(EntidadDAO entidadDAO) {
		this.entidadDAO = entidadDAO;
	}

}
