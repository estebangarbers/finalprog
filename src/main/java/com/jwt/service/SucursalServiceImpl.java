package com.jwt.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jwt.dao.SucursalDAO;
import com.jwt.model.Sucursal;

@Service
@Transactional
public class SucursalServiceImpl implements SucursalService {

    @Autowired
    private SucursalDAO sucursalDAO;

    @Override
    @Transactional
    public void addSucursal(Sucursal sucursal) {
            sucursalDAO.addSucursal(sucursal);
    }

    @Override
    @Transactional
    public List<Sucursal> getAllSucursales() {
            return sucursalDAO.getAllSucursales();
    }

    @Override
    @Transactional
    public void deleteSucursal(Integer sucursalId) {
            sucursalDAO.deleteSucursal(sucursalId);
    }
    
    @Override
    public Sucursal getSucursal(int sucursalid) {
            return sucursalDAO.getSucursal(sucursalid);
    }
    
    @Override
    public Long countEmpleados(int sucursalid) {
            return sucursalDAO.countEmpleados(sucursalid);
    }
    
    @Override
    public boolean checkSucursalByDomicilio(String domicilio) {
            return sucursalDAO.checkSucursalByDomicilio(domicilio);
    }

    @Override
    public Sucursal updateSucursal(Sucursal sucursal) {
            return sucursalDAO.updateSucursal(sucursal);
    }

    public void setSucursalDAO(SucursalDAO sucursalDAO) {
            this.sucursalDAO = sucursalDAO;
    }

}
