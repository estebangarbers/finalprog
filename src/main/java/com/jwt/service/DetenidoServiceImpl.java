package com.jwt.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jwt.dao.DetenidoDAO;
import com.jwt.model.Detenido;

@Service
@Transactional
public class DetenidoServiceImpl implements DetenidoService {

    @Autowired
    private DetenidoDAO detenidoDAO;

    @Override
    @Transactional
    public void addDetenido(Detenido detenido) {
            detenidoDAO.addDetenido(detenido);
    }

    @Override
    @Transactional
    public List<Detenido> getAllDetenidos() {
            return detenidoDAO.getAllDetenidos();
    }
    
    @Override
    @Transactional
    public List<Detenido> getAllBandas() {
            return detenidoDAO.getAllBandas();
    }

    @Override
    @Transactional
    public void deleteDetenido(Integer detenidoId) {
            detenidoDAO.deleteDetenido(detenidoId);
    }
    
    @Override
    public Detenido getDetenido(int detenidoid) {
            return detenidoDAO.getDetenido(detenidoid);
    }
    
    @Override
    public int countMiembros(String banda) {
            return detenidoDAO.countMiembros(banda);
    }

    @Override
    public Detenido updateDetenido(Detenido detenido) {
            return detenidoDAO.updateDetenido(detenido);
    }

    public void setDetenidoDAO(DetenidoDAO detenidoDAO) {
            this.detenidoDAO = detenidoDAO;
    }

}
