package com.jwt.service;

import java.util.List;

import com.jwt.model.Usuario;
import java.util.Date;

public interface FechaService {
    public int calculateAge(String birthDate, Date currentDate);
}
