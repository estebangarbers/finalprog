package com.jwt.service;

import java.util.List;

import com.jwt.model.Usuario;

public interface UsuarioService {
	
	public void addUsuario(Usuario usuario);

	public List<Usuario> getAllUsuarios();

	public void deleteUsuario(Integer usuarioId);

	public Usuario getUsuario(int usuarioid);
        
        public List<Usuario> getUsuarioByUserAndPassword(String user, String password);
        
        public boolean getUsuarioByUser(String user);
        
        public List<Usuario> getAllUsuariosByRol(String rol);

	public Usuario updateUsuario(Usuario usuario);
}
