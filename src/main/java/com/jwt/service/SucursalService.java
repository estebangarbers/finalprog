package com.jwt.service;

import java.util.List;

import com.jwt.model.Sucursal;

public interface SucursalService {
	
	public void addSucursal(Sucursal sucursal);

	public List<Sucursal> getAllSucursales();

	public void deleteSucursal(Integer sucursalId);

	public Sucursal getSucursal(int sucursalid);
        
        public Long countEmpleados(int sucursalid);
        
        public boolean checkSucursalByDomicilio(String domicilio);

	public Sucursal updateSucursal(Sucursal sucursal);
}
