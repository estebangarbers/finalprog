package com.jwt.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jwt.dao.ContratoDAO;
import com.jwt.model.Contrato;

@Service
@Transactional
public class ContratoServiceImpl implements ContratoService {

	@Autowired
	private ContratoDAO contratoDAO;

	@Override
	@Transactional
	public void addContrato(Contrato contrato) {
		contratoDAO.addContrato(contrato);
	}

	@Override
	@Transactional
	public List<Contrato> getAllContratos() {
		return contratoDAO.getAllContratos();
	}

	@Override
	@Transactional
	public void deleteContrato(Integer contratoId) {
		contratoDAO.deleteContrato(contratoId);
	}

        @Override
	public Contrato getContrato(int contratoid) {
		return contratoDAO.getContrato(contratoid);
	}
        
        @Override
        public List<Contrato> getAllContratosByVigilante(int vigilante) {
		return contratoDAO.getAllContratosByVigilante(vigilante);
	}
        
        @Override
        public int getAllContratosBySucursal(int sucursal) {
		return contratoDAO.getAllContratosBySucursal(sucursal);
	}
        
        @Override
        public boolean checkContrato(String fecha, int vigilante) {
            return contratoDAO.checkContrato(fecha, vigilante);
	}

        @Override
	public Contrato updateContrato(Contrato contrato) {
		return contratoDAO.updateContrato(contrato);
	}

	public void setContratoDAO(ContratoDAO contratoDAO) {
		this.contratoDAO = contratoDAO;
	}

}
