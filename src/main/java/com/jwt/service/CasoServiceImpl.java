package com.jwt.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jwt.dao.CasoDAO;
import com.jwt.model.Caso;

@Service
@Transactional
public class CasoServiceImpl implements CasoService {

	@Autowired
	private CasoDAO casoDAO;

	@Override
	@Transactional
	public void addCaso(Caso caso) {
            casoDAO.addCaso(caso);
	}

	@Override
	@Transactional
	public List<Caso> getAllCasos() {
            return casoDAO.getAllCasos();
	}

        @Override
	@Transactional
	public void deleteCaso(Integer casoId) {
            casoDAO.deleteCaso(casoId);
	}

        @Override
	public Caso getCaso(int casoid) {
            return casoDAO.getCaso(casoid);
	}
        
        @Override
        public boolean checkCaso(String fecha, int detenido) {
            return casoDAO.checkCaso(fecha, detenido);
	}

        @Override
	public Caso updateCaso(Caso caso) {
            return casoDAO.updateCaso(caso);
	}

	public void setCasoDAO(CasoDAO casoDAO) {
            this.casoDAO = casoDAO;
	}
        
        @Override
        public List<Caso> getAllCasosByDetenido(int detenido) {
		return casoDAO.getAllCasosByDetenido(detenido);
	}

}
