package com.jwt.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jwt.dao.UsuarioDAO;
import com.jwt.model.Usuario;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
@Transactional
public class FechaServiceImpl implements FechaService {

        @Override
        public int calculateAge(
            String birthDate, 
            Date currentDate) {            
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            long age =0;
            try {
                Date d = sdf.parse(birthDate);
            } catch (ParseException ex) {
                Logger.getLogger(FechaServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
            }              
              Date firstDate;
            try {
                firstDate = sdf.parse(birthDate);
                long diffInMillies = Math.abs(currentDate.getTime() - firstDate.getTime());
                age = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
            } catch (ParseException ex) {
                Logger.getLogger(FechaServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
            return (int) age/365;                                                                        
        }

}
