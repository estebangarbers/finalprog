package com.jwt.service;

import java.util.List;

import com.jwt.model.Detenido;

public interface DetenidoService {
	
	public void addDetenido(Detenido detenido);

	public List<Detenido> getAllDetenidos();
        
        public List<Detenido> getAllBandas();

	public void deleteDetenido(Integer detenidoId);
        
        public int countMiembros(String banda);

	public Detenido getDetenido(int detenidoid);

	public Detenido updateDetenido(Detenido detenido);
}
