package com.jwt.service;

import java.util.List;

import com.jwt.model.Caso;

public interface CasoService {
	
	public void addCaso(Caso caso);

	public List<Caso> getAllCasos();

	public void deleteCaso(Integer casoId);

	public Caso getCaso(int casoid);
        
        public boolean checkCaso(String fecha, int detenido);

	public Caso updateCaso(Caso caso);
        
        public List<Caso> getAllCasosByDetenido(int detenido);
}
