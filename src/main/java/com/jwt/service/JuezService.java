package com.jwt.service;

import java.util.List;

import com.jwt.model.Juez;
import java.util.Date;

public interface JuezService {
	
	public void addJuez(Juez juez);

	public List<Juez> getAllJueces();

	public void deleteJuez(Integer juezId);

	public Juez getJuez(int juezid);

	public Juez updateJuez(Juez juez);
        
        public int calculateAge(Date fecha, Date actual);

        //public void calculateAgeWithJava7(String string, String string0);
}
