package com.jwt.service;

import java.util.List;

import com.jwt.model.Entidad;

public interface EntidadService {
	
	public void addEntidad(Entidad entidad);

	public List<Entidad> getAllEntidades();

	public void deleteEntidad(Integer entidadId);

	public Entidad getEntidad(int entidadid);
        
        public boolean checkEntidadByDomicilio(String domicilio);

	public Entidad updateEntidad(Entidad entidad);
}
