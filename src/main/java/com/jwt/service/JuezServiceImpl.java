package com.jwt.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jwt.dao.JuezDAO;
import com.jwt.model.Juez;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@Service
@Transactional
public class JuezServiceImpl implements JuezService {

	@Autowired
	private JuezDAO juezDAO;

	@Override
	@Transactional
	public void addJuez(Juez juez) {
		juezDAO.addJuez(juez);
	}

	@Override
	@Transactional
	public List<Juez> getAllJueces() {
		return juezDAO.getAllJueces();
	}

	@Override
	@Transactional
	public void deleteJuez(Integer juezId) {
		juezDAO.deleteJuez(juezId);
	}

        @Override
	public Juez getJuez(int juezid) {
		return juezDAO.getJuez(juezid);
	}

        @Override
	public Juez updateJuez(Juez juez) {
		// TODO Auto-generated method stub
		return juezDAO.updateJuez(juez);
	}

	public void setJuezDAO(JuezDAO juezDAO) {
		this.juezDAO = juezDAO;
	}
        
        @Override
        public int calculateAge(
            Date birthDate, 
            Date currentDate) {            
              // validate inputs ...                                                                               
              DateFormat formatter = new SimpleDateFormat("yyyyMMdd");                           
              int d1 = Integer.parseInt(formatter.format(birthDate));                            
              int d2 = Integer.parseInt(formatter.format(currentDate));                          
              int age = (d2 - d1) / 10000;                                                       
              return age;                                                                        
        }

}
