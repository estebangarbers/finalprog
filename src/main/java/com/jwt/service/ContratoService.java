package com.jwt.service;

import java.util.List;

import com.jwt.model.Contrato;

public interface ContratoService {
	
	public void addContrato(Contrato contrato);

	public List<Contrato> getAllContratos();

	public void deleteContrato(Integer contratoId);

	public Contrato getContrato(int contratoid);
        
        public List<Contrato> getAllContratosByVigilante(int vigilante);
        
        public int getAllContratosBySucursal(int sucursal);
        
        public boolean checkContrato(String fecha, int vigilante);

	public Contrato updateContrato(Contrato contrato);
}
