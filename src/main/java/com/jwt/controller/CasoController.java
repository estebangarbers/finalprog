package com.jwt.controller;

import com.jwt.model.Caso;
import com.jwt.model.Employee;
import com.jwt.model.Entidad;
import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.jwt.model.Detenido;
import com.jwt.service.DetenidoService;
import com.jwt.model.Entidad;
import com.jwt.model.Juez;
import com.jwt.model.Sucursal;
import com.jwt.service.CasoService;
import com.jwt.service.EntidadService;
import com.jwt.service.JuezService;
import com.jwt.service.SucursalService;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class CasoController {

    private static final Logger logger = Logger
                    .getLogger(CasoController.class);

    public CasoController() {
            System.out.println("CasoController()");
    }

    @Autowired
    private DetenidoService detenidoService;
    @Autowired
    private CasoService casoService;
    @Autowired
    private SucursalService sucursalService;
    @Autowired
    private JuezService juezService;

    @RequestMapping(value = "/casos")
    public ModelAndView listCasos(ModelAndView model) throws IOException {
        List<Caso> listCasos = casoService.getAllCasos();
        model.addObject("listCasos", listCasos);
        model.setViewName("casos");
        return model;
    }

    @RequestMapping(value = "/casos/nuevo")
    public ModelAndView nuevoCaso(ModelAndView model) throws IOException {
        List<Detenido> listDetenidos = detenidoService.getAllDetenidos();
        List<Sucursal> listSucursales = sucursalService.getAllSucursales();
        List<Juez> listJueces = juezService.getAllJueces();
        model.addObject("listDetenidos", listDetenidos);
        model.addObject("listSucursales", listSucursales);
        model.addObject("listJueces", listJueces);
        Caso caso = new Caso();
        model.addObject("caso", caso);
        model.setViewName("casoForm");
        return model;
    }
   
    @RequestMapping(value = "/casos/save", method = RequestMethod.POST)
    public String saveCaso(HttpServletRequest request, RedirectAttributes redirectAttributes) {
        if (request.getParameter("id").equals("0")) {
            Caso caso = new Caso();
            if (request.getParameter("hubocondena").equals("0")) {
                caso.setCondena(0);
            }else{
                caso.setCondena(Integer.parseInt(request.getParameter("condena")));
            }
            Detenido detenido = detenidoService.getDetenido(Integer.parseInt(request.getParameter("detenido")));
            caso.setDetenido(detenido);
            Juez juez = juezService.getJuez(Integer.parseInt(request.getParameter("juez")));
            caso.setJuez(juez);
            caso.setFecha(request.getParameter("fecha"));
            Sucursal sucursal = sucursalService.getSucursal(Integer.parseInt(request.getParameter("sucursal")));
            caso.setSucursal(sucursal);
            casoService.addCaso(caso);
            redirectAttributes.addFlashAttribute("msg", "<div class='alert alert-success'>El caso fue registrado correctamente.</div>");
        } else {
            Caso caso = casoService.getCaso(Integer.parseInt(request.getParameter("id")));
            if (request.getParameter("hubocondena").equals("0")) {
                caso.setCondena(0);
            }else{
                caso.setCondena(Integer.parseInt(request.getParameter("condena")));
            }
            Detenido detenido = detenidoService.getDetenido(Integer.parseInt(request.getParameter("detenido")));
            caso.setDetenido(detenido);
            Juez juez = juezService.getJuez(Integer.parseInt(request.getParameter("juez")));
            caso.setJuez(juez);
            caso.setFecha(request.getParameter("fecha"));
            Sucursal sucursal = sucursalService.getSucursal(Integer.parseInt(request.getParameter("sucursal")));
            caso.setSucursal(sucursal);
            casoService.updateCaso(caso);
            redirectAttributes.addFlashAttribute("msg", "<div class='alert alert-success'>El caso fue modificado correctamente.</div>");              
        }        
        return "redirect:/casos";
    }
 
    @RequestMapping(value = "/casos/editar", method = RequestMethod.GET)
    public ModelAndView editCaso(HttpServletRequest request) {
        List<Detenido> listDetenidos = detenidoService.getAllDetenidos();
        List<Sucursal> listSucursales = sucursalService.getAllSucursales();
        List<Juez> listJueces = juezService.getAllJueces();       
        
        int casoId = Integer.parseInt(request.getParameter("id"));
        Caso caso = casoService.getCaso(casoId);
        ModelAndView model = new ModelAndView("casoForm");
        model.addObject("caso", caso);
        model.addObject("listDetenidos", listDetenidos);
        model.addObject("listSucursales", listSucursales);
        model.addObject("listJueces", listJueces);
        return model;
    }

    @RequestMapping(value = "/casos/eliminar", method = RequestMethod.GET)
    public String deleteDetenido(HttpServletRequest request, RedirectAttributes redirectAttributes) {
            int casoId = Integer.parseInt(request.getParameter("id"));
            casoService.deleteCaso(casoId);
            redirectAttributes.addFlashAttribute("msg", "<div class='alert alert-success'>El caso fue eliminado correctamente.</div>");
            return "redirect:/casos";
    }
}