package com.jwt.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.jwt.model.Usuario;
import com.jwt.service.UsuarioService;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class LoginController {

	private static final Logger logger = Logger
			.getLogger(LoginController.class);

	public LoginController() {
		System.out.println("UsuarioController()");
	}

	@Autowired
	private UsuarioService usuarioService;

        
        @RequestMapping(value = "/")
	public ModelAndView login(ModelAndView model) throws IOException {
		model.setViewName("login");
		return model;
	}
        
        @RequestMapping(value = "/home")
	public ModelAndView home(ModelAndView model) throws IOException {
		model.setViewName("home");
		return model;
	}          
        
        @RequestMapping(value = "/checkAuth", method = RequestMethod.POST)
	public String checkAuth(HttpServletRequest request, RedirectAttributes redirectAttributes) {
		String user = request.getParameter("user");
                String password = request.getParameter("password");
		List<Usuario> usuario = usuarioService.getUsuarioByUserAndPassword(user, password);
		
                if(!usuario.isEmpty())
                {
                    //ModelAndView model = new ModelAndView("redirect:/home");
                    request.getSession().setAttribute("userName",usuario.get(0).getNombre());       
                    request.getSession().setAttribute("userRol",usuario.get(0).getRol());  
                    if(!usuario.get(0).getRol().equals("Vigilante"))
                        return "redirect:/home";
                    else
                        return "redirect:/vigilante?id="+usuario.get(0).getId();
                }
                else
                {
                    redirectAttributes.addFlashAttribute("msg", "<div class='alert alert-danger'>Usuario y/o contrase�a incorrecta.</div>");
                    return "redirect:/";
                }                
	}
        
        @RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logout(HttpServletRequest request, RedirectAttributes redirectAttributes) {
            request.getSession().removeAttribute("userName");
            request.getSession().removeAttribute("userRol");
            redirectAttributes.addFlashAttribute("msg", "<div class='alert alert-success'>Ha cerrado sesi�n correctamente.</div>");
            return "redirect:/";
	}

}