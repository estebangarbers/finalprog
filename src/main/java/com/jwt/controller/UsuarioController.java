package com.jwt.controller;

import com.jwt.model.Contrato;
import com.jwt.model.Entidad;
import com.jwt.model.Sucursal;
import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.jwt.model.Usuario;
import com.jwt.service.UsuarioService;
import com.jwt.model.Contrato;
import com.jwt.service.ContratoService;
import com.jwt.service.SucursalService;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class UsuarioController {

	private static final Logger logger = Logger
			.getLogger(UsuarioController.class);

	public UsuarioController() {
		System.out.println("UsuarioController()");
	}

	@Autowired
	private UsuarioService usuarioService;
        
        @Autowired
	private ContratoService contratoService;
        
        @Autowired
	private SucursalService sucursalService;
        
        @RequestMapping(value = "/vigilantes")
        public ModelAndView listVigilantes(ModelAndView model) throws IOException {
            List<Usuario> listVigilantes = usuarioService.getAllUsuariosByRol("Vigilante");
            model.addObject("listVigilantes", listVigilantes);
            model.setViewName("vigilantes");
            return model;
        }
        
        @RequestMapping(value = "/investigadores")
        public ModelAndView listInvestigadores(ModelAndView model) throws IOException {
            List<Usuario> listInvestigadores = usuarioService.getAllUsuariosByRol("Investigador");
            model.addObject("listInvestigadores", listInvestigadores);
            model.setViewName("investigadores");
            return model;
        }
        
        @RequestMapping(value = "/usuarios/nuevo", method = RequestMethod.GET)
        public ModelAndView nuevoUsuario(ModelAndView model, HttpServletRequest request) throws IOException {
            Usuario usuario = new Usuario();
            model.addObject("usuario", usuario);
            model.addObject("tRol", request.getParameter("tRol"));
            model.setViewName("usuarioForm");
            return model;
        }
        
        @RequestMapping(value = "/usuarios/save", method = RequestMethod.POST)
        public String saveUsuario(@ModelAttribute Usuario usuario, RedirectAttributes redirectAttributes) {
            boolean checkUser = usuarioService.getUsuarioByUser(usuario.getUsuario());
            
            if (usuario.getId() == 0){
                if (checkUser){
                    usuarioService.addUsuario(usuario);
                    redirectAttributes.addFlashAttribute("msg", "<div class='alert alert-success'>El usuario ha sido registrado correctamente.</div>");
                }else{
                    redirectAttributes.addFlashAttribute("msg", "<div class='alert alert-danger'>Ya existe un usuario con el nombre de usuario <b>"+usuario.getUsuario()+"</b>, no puede repetirlo.</div>");
                }                
            } else {
                Usuario usuarioSel = usuarioService.getUsuario(usuario.getId());
                if (!checkUser && !usuarioSel.getUsuario().equals(usuario.getUsuario())) {
                    redirectAttributes.addFlashAttribute("msg", "<div class='alert alert-danger'>Ya existe un usuario con el nombre de usuario <b>"+usuario.getUsuario()+"</b>, no puede repetirlo.</div>");
                }else{
                    usuarioService.updateUsuario(usuario);
                    redirectAttributes.addFlashAttribute("msg", "<div class='alert alert-success'>El usuario <b>"+usuario.getNombre()+"</b> ha sido modificado correctamente.</div>");
                }                
            }

            if(usuario.getRol().equals("Administrador")){
                return "redirect:/usuarios";
            }else if(usuario.getRol().equals("Vigilante")){
                return "redirect:/vigilantes";
            }else{
                return "redirect:/investigadores";
            }
        }
        
        @RequestMapping(value = "/usuarios/editar", method = RequestMethod.GET)
        public ModelAndView editUsuario(HttpServletRequest request) {
            int usuarioId = Integer.parseInt(request.getParameter("id"));
            Usuario usuario = usuarioService.getUsuario(usuarioId);
            ModelAndView model = new ModelAndView("usuarioForm");
            model.addObject("tRol", request.getParameter("tRol"));      
            model.addObject("usuario", usuario);
            return model;
        }
        
        @RequestMapping(value = "/vigilante/contratos", method = RequestMethod.GET)
        public ModelAndView vigilanteContratos(HttpServletRequest request) {
            int usuarioId = Integer.parseInt(request.getParameter("id"));
            Usuario usuario = usuarioService.getUsuario(usuarioId);
            List<Contrato> listContratos = contratoService.getAllContratosByVigilante(usuarioId);
            ModelAndView model = new ModelAndView("vigilanteContratos");          
            model.addObject("usuario", usuario);
            model.addObject("contratos", listContratos);
            return model;
        }
        
        @RequestMapping(value = "/vigilante", method = RequestMethod.GET)
        public ModelAndView vigilante(HttpServletRequest request) {
            int usuarioId = Integer.parseInt(request.getParameter("id"));
            Usuario usuario = usuarioService.getUsuario(usuarioId);
            List<Contrato> listContratos = contratoService.getAllContratosByVigilante(usuarioId);
            ModelAndView model = new ModelAndView("vigilante");          
            model.addObject("usuario", usuario);
            model.addObject("contratos", listContratos);
            return model;
        }
        
        @RequestMapping(value = "/vigilante/contratos/nuevo", method = RequestMethod.GET)
        public ModelAndView nuevoContrato(ModelAndView model, HttpServletRequest request) throws IOException {
            List<Sucursal> listSucursales = sucursalService.getAllSucursales();
            int usuarioId = Integer.parseInt(request.getParameter("id"));
            Usuario usuario = usuarioService.getUsuario(usuarioId);
            Contrato contrato = new Contrato();
            model.addObject("contrato", contrato);
            model.addObject("usuario", usuario);
            model.addObject("listSucursales", listSucursales);
            model.setViewName("contratoForm");
            return model;
        }
        
        @RequestMapping(value = "/vigilante/contratos/save", method = RequestMethod.POST)
        public String saveContrato(HttpServletRequest request, RedirectAttributes redirectAttributes) {
            boolean checkContrato = contratoService.checkContrato(request.getParameter("fecha"), Integer.parseInt(request.getParameter("usuarioId")));
            
                if (request.getParameter("id").equals("0")) {
                    if (checkContrato) {
                        Contrato contrato = new Contrato();
                        contrato.setFecha(request.getParameter("fecha"));
                        contrato.setArmado(Integer.parseInt(request.getParameter("armado")));
                        Sucursal sucursal = sucursalService.getSucursal(Integer.parseInt(request.getParameter("sucursal")));
                        contrato.setSucursal(sucursal);
                        Usuario usuario = usuarioService.getUsuario(Integer.parseInt(request.getParameter("usuarioId")));
                        contrato.setUsuarioId(Integer.parseInt(request.getParameter("usuarioId")));
                        contratoService.addContrato(contrato);
                        redirectAttributes.addFlashAttribute("msg", "<div class='alert alert-success'>El contrato ha sido registrado correctamente.</div>");
                    }else{
                        redirectAttributes.addFlashAttribute("msg", "<div class='alert alert-danger'>El contrato no pudo ser registrado, ya existe uno registrado el d�a <b>"+request.getParameter("fecha")+"</b>.</div>");
                    }                    
                } else {
                    Contrato contrato = contratoService.getContrato(Integer.parseInt(request.getParameter("id")));
                    if (!checkContrato && !contrato.getFecha().equals(request.getParameter("fecha"))) {
                        redirectAttributes.addFlashAttribute("msg", "<div class='alert alert-danger'>El contrato no pudo ser registrado, ya existe uno registrado el d�a <b>"+request.getParameter("fecha")+"</b>.</div>");
                    }else{
                        contrato.setFecha(request.getParameter("fecha"));
                        contrato.setArmado(Integer.parseInt(request.getParameter("armado")));
                        Sucursal sucursal = sucursalService.getSucursal(Integer.parseInt(request.getParameter("sucursal")));
                        contrato.setSucursal(sucursal);
                        Usuario usuario = usuarioService.getUsuario(Integer.parseInt(request.getParameter("usuarioId")));
                        contrato.setUsuarioId(Integer.parseInt(request.getParameter("usuarioId")));
                        contratoService.updateContrato(contrato);
                        redirectAttributes.addFlashAttribute("msg", "<div class='alert alert-success'>El contrato ha sido modificado correctamente.</div>");
                    }                    
                }
                
                return "redirect:/vigilante/contratos?id="+request.getParameter("usuarioId");
        }
        
        @RequestMapping(value = "/vigilante/contratos/editar", method = RequestMethod.GET)
        public ModelAndView editContrato(HttpServletRequest request) {
            List<Sucursal> listSucursales = sucursalService.getAllSucursales();
            int contratoId = Integer.parseInt(request.getParameter("id"));
            Contrato contrato = contratoService.getContrato(contratoId);
            Usuario usuario = usuarioService.getUsuario(contrato.getUsuarioId());
            ModelAndView model = new ModelAndView("contratoForm");
            model.addObject("contrato", contrato);      
            model.addObject("usuario", usuario);
            model.addObject("listSucursales", listSucursales);
            return model;
        }
        
        @RequestMapping(value = "/vigilante/contratos/eliminar", method = RequestMethod.GET)
        public String deleteContrato(HttpServletRequest request, RedirectAttributes redirectAttributes) {
                int contratoId = Integer.parseInt(request.getParameter("id"));
                contratoService.deleteContrato(contratoId);
                redirectAttributes.addFlashAttribute("msg", "<div class='alert alert-success'>El contrato fue eliminado correctamente.</div>");
                return "redirect:/vigilante/contratos?id="+request.getParameter("idVigilante");
        }
        
        @RequestMapping(value = "/usuarios/eliminar", method = RequestMethod.GET)
        public String deleteUsuario(HttpServletRequest request, RedirectAttributes redirectAttributes) {
            int usuarioId = Integer.parseInt(request.getParameter("id"));
            usuarioService.deleteUsuario(usuarioId);
            redirectAttributes.addFlashAttribute("msg", "<div class='alert alert-success'>El usuario fue eliminado correctamente.</div>");

            if(request.getParameter("tRol").equals("investigadores")){
               return "redirect:/investigadores";
            }else{
                return "redirect:/vigilantes";
            }
        }
                
}