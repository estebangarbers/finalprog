package com.jwt.controller;

import com.jwt.model.Employee;
import com.jwt.model.Entidad;
import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.jwt.model.Sucursal;
import com.jwt.service.SucursalService;
import com.jwt.model.Entidad;
import com.jwt.service.ContratoService;
import com.jwt.service.EntidadService;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class SucursalController {

    private static final Logger logger = Logger
                    .getLogger(SucursalController.class);

    public SucursalController() {
            System.out.println("SucursalController()");
    }

    @Autowired
    private SucursalService sucursalService;
    @Autowired
    private EntidadService entidadService;
    @Autowired
    private ContratoService contratoService;

    @RequestMapping(value = "/sucursales")
    public ModelAndView listSucursales(ModelAndView model) throws IOException {
        List<Sucursal> listSucursales = sucursalService.getAllSucursales();
        for(Sucursal sucursal : listSucursales){
            sucursal.setEmpleados(contratoService.getAllContratosBySucursal(sucursal.getId()));
        }
        model.addObject("listSucursales", listSucursales);
        model.setViewName("sucursales");
        return model;
    }

    @RequestMapping(value = "/sucursales/nueva")
    public ModelAndView nuevaSucursal(ModelAndView model) throws IOException {
        List<Entidad> listEntidades = entidadService.getAllEntidades();
        model.addObject("listEntidades", listEntidades);
        Sucursal sucursal = new Sucursal();
        model.addObject("sucursal", sucursal);
        model.setViewName("sucursalForm");
        return model;
    }

    @RequestMapping(value = "/sucursales/save", method = RequestMethod.POST)
    public String saveSucursal(HttpServletRequest request, RedirectAttributes redirectAttributes) {
        boolean checkDomicilio = sucursalService.checkSucursalByDomicilio(request.getParameter("domicilio"));
        //sucursal.getEntidad();
        if (request.getParameter("id").equals("0")) {
            if (checkDomicilio) {
                Sucursal sucursal = new Sucursal();
                sucursal.setDomicilio(request.getParameter("domicilio"));
                Entidad entidad = entidadService.getEntidad(Integer.parseInt(request.getParameter("entidad")));
                sucursal.setEntidad(entidad);
                sucursalService.addSucursal(sucursal);
                redirectAttributes.addFlashAttribute("msg", "<div class='alert alert-success'>La sucursal fue creada correctamente.</div>");
            }else{
                redirectAttributes.addFlashAttribute("msg", "<div class='alert alert-danger'>La sucursal no pudo ser registrada, ya existe el domicilio <b>"+request.getParameter("domicilio")+"</b>.</div>");
            }
        } else {
            Sucursal sucursal = sucursalService.getSucursal(Integer.parseInt(request.getParameter("id")));
            if (!checkDomicilio && !sucursal.getDomicilio().equals(request.getParameter("domicilio"))) {
                redirectAttributes.addFlashAttribute("msg", "<div class='alert alert-danger'>La sucursal no pudo ser registrada, ya existe el domicilio <b>"+request.getParameter("domicilio")+"</b>.</div>");
            }else{
                sucursal.setDomicilio(request.getParameter("domicilio"));
                Entidad entidad = entidadService.getEntidad(Integer.parseInt(request.getParameter("entidad")));
                sucursal.setEntidad(entidad);
                sucursalService.updateSucursal(sucursal);
                redirectAttributes.addFlashAttribute("msg", "<div class='alert alert-success'>La sucursal fue editada correctamente.</div>");
            }                
        }

        return "redirect:/sucursales";
    }

    @RequestMapping(value = "/sucursales/editar", method = RequestMethod.GET)
    public ModelAndView editSucursal(HttpServletRequest request) {
        List<Entidad> listEntidades = entidadService.getAllEntidades();
        
        int sucursalId = Integer.parseInt(request.getParameter("id"));
        Sucursal sucursal = sucursalService.getSucursal(sucursalId);
        ModelAndView model = new ModelAndView("sucursalForm");
        model.addObject("sucursal", sucursal);
        model.addObject("listEntidades", listEntidades);
        return model;
    }

    @RequestMapping(value = "/sucursales/eliminar", method = RequestMethod.GET)
    public String deleteSucursal(HttpServletRequest request, RedirectAttributes redirectAttributes) {
            int sucursalId = Integer.parseInt(request.getParameter("id"));
            sucursalService.deleteSucursal(sucursalId);
            redirectAttributes.addFlashAttribute("msg", "<div class='alert alert-success'>La sucursal fue eliminada correctamente.</div>");
            return "redirect:/sucursales";
    }
}