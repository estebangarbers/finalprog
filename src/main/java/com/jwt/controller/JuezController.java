package com.jwt.controller;

import com.jwt.model.Contrato;
import com.jwt.model.Entidad;
import com.jwt.model.Sucursal;
import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.jwt.model.Juez;
import com.jwt.service.JuezService;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class JuezController {

	private static final Logger logger = Logger
			.getLogger(JuezController.class);

	public JuezController() {
		System.out.println("JuezController()");
	}

	@Autowired
	private JuezService juezService;
        
        @RequestMapping(value = "/jueces")
        public ModelAndView listJueces(ModelAndView model) throws IOException, ParseException {
            List<Juez> listJueces = juezService.getAllJueces();
            model.addObject("listJueces", listJueces);
            model.setViewName("jueces");
            return model;
        }
        
        @RequestMapping(value = "/jueces/nuevo", method = RequestMethod.GET)
        public ModelAndView nuevoJuez(ModelAndView model, HttpServletRequest request) throws IOException {
            Juez juez = new Juez();
            model.addObject("juez", juez);
            model.setViewName("juezForm");
            return model;
        }
        
        @RequestMapping(value = "/jueces/save", method = RequestMethod.POST)
        public String saveJuez(@ModelAttribute Juez juez, RedirectAttributes redirectAttributes) {            
            if (juez.getId() == 0){
                juezService.addJuez(juez);
                redirectAttributes.addFlashAttribute("msg", "<div class='alert alert-success'>El Juez <b>"+juez.getNombre()+"</b> ha sido registrado correctamente.</div>");             
            } else {
                juezService.updateJuez(juez);
                redirectAttributes.addFlashAttribute("msg", "<div class='alert alert-success'>El Juez <b>"+juez.getNombre()+"</b> ha sido modificado correctamente.</div>");              
            }

            return "redirect:/jueces";
        }
        
        @RequestMapping(value = "/jueces/editar", method = RequestMethod.GET)
        public ModelAndView editJuez(HttpServletRequest request) {
            int juezId = Integer.parseInt(request.getParameter("id"));
            Juez juez = juezService.getJuez(juezId);
            ModelAndView model = new ModelAndView("juezForm");    
            model.addObject("juez", juez);
            return model;
        }
        
        @RequestMapping(value = "/jueces/eliminar", method = RequestMethod.GET)
        public String deleteJuez(HttpServletRequest request, RedirectAttributes redirectAttributes) {
            int juezId = Integer.parseInt(request.getParameter("id"));
            juezService.deleteJuez(juezId);
            redirectAttributes.addFlashAttribute("msg", "<div class='alert alert-success'>El juez fue eliminado correctamente.</div>");
            return "redirect:/jueces";
        }
}