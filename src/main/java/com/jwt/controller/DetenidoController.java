package com.jwt.controller;

import com.jwt.model.Caso;
import com.jwt.model.Employee;
import com.jwt.model.Entidad;
import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.jwt.model.Detenido;
import com.jwt.service.DetenidoService;
import com.jwt.model.Entidad;
import com.jwt.service.CasoService;
import com.jwt.service.EntidadService;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class DetenidoController {

    private static final Logger logger = Logger
                    .getLogger(DetenidoController.class);

    public DetenidoController() {
            System.out.println("DetenidoController()");
    }

    @Autowired
    private DetenidoService detenidoService;
    @Autowired
    private EntidadService entidadService;
    @Autowired
    private CasoService casoService;

    @RequestMapping(value = "/detenidos")
    public ModelAndView listDetenidos(ModelAndView model) throws IOException {
        List<Detenido> listDetenidos = detenidoService.getAllDetenidos();
        for(Detenido detenido : listDetenidos){
            detenido.setMiembros(detenidoService.countMiembros(detenido.getBanda()));
        }
        
        model.addObject("listDetenidos", listDetenidos);
        model.setViewName("detenidos");
        return model;
    }

    @RequestMapping(value = "/detenidos/nuevo")
    public ModelAndView nuevoDetenido(ModelAndView model) throws IOException {
        List<Detenido> listBandas = detenidoService.getAllBandas();
        model.addObject("listBandas", listBandas);
        Detenido detenido = new Detenido();
        model.addObject("detenido", detenido);
        model.setViewName("detenidoForm");
        return model;
    }

    @RequestMapping(value = "/detenidos/save", method = RequestMethod.POST)
    public String saveDetenido(@ModelAttribute Detenido detenido, HttpServletRequest request, RedirectAttributes redirectAttributes) {
        if (detenido.getId() == 0) {
            if (detenido.getBanda().equals("0")) {
                detenido.setBanda(request.getParameter("nombreBanda"));
            }
            detenidoService.addDetenido(detenido);
            redirectAttributes.addFlashAttribute("msg", "<div class='alert alert-success'>El detenido fue registrado correctamente.</div>");
        } else {
            if (detenido.getBanda().equals("0")) {
                detenido.setBanda(request.getParameter("nombreBanda"));
            }
            detenidoService.updateDetenido(detenido);
            redirectAttributes.addFlashAttribute("msg", "<div class='alert alert-success'>El detenido fue modificado correctamente.</div>");              
        }        
        return "redirect:/detenidos";
    }

    @RequestMapping(value = "/detenidos/editar", method = RequestMethod.GET)
    public ModelAndView editDetenido(HttpServletRequest request) {
        List<Detenido> listBandas = detenidoService.getAllBandas();
        
        int detenidoId = Integer.parseInt(request.getParameter("id"));
        Detenido detenido = detenidoService.getDetenido(detenidoId);
        ModelAndView model = new ModelAndView("detenidoForm");
        model.addObject("detenido", detenido);
        model.addObject("listBandas", listBandas);
        return model;
    }

    @RequestMapping(value = "/detenidos/eliminar", method = RequestMethod.GET)
    public String deleteDetenido(HttpServletRequest request, RedirectAttributes redirectAttributes) {
            int detenidoId = Integer.parseInt(request.getParameter("id"));
            detenidoService.deleteDetenido(detenidoId);
            redirectAttributes.addFlashAttribute("msg", "<div class='alert alert-success'>El detenido fue eliminado correctamente.</div>");
            return "redirect:/detenidos";
    }
    
    @RequestMapping(value = "/detenido/casos", method = RequestMethod.GET)
    public ModelAndView detenidoCasos(HttpServletRequest request) {
        int detenidoId = Integer.parseInt(request.getParameter("id"));
        Detenido detenido = detenidoService.getDetenido(detenidoId);
        List<Caso> listCasos = casoService.getAllCasosByDetenido(detenidoId);
        ModelAndView model = new ModelAndView("detenidoCasos");          
        model.addObject("detenido", detenido);
        model.addObject("listCasos", listCasos);
        return model;
    }
}