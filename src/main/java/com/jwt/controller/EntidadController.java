package com.jwt.controller;

import com.jwt.model.Employee;
import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.jwt.model.Entidad;
import com.jwt.service.EntidadService;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class EntidadController {

    private static final Logger logger = Logger
                    .getLogger(EntidadController.class);

    public EntidadController() {
            System.out.println("EntidadController()");
    }

    @Autowired
    private EntidadService entidadService;

    @RequestMapping(value = "/entidades")
    public ModelAndView listEntidades(ModelAndView model) throws IOException {
        List<Entidad> listEntidades = entidadService.getAllEntidades();
        model.addObject("listEntidades", listEntidades);
        model.setViewName("entidades");
        return model;
    }

    @RequestMapping(value = "/entidades/nueva")
    public ModelAndView nuevaEntidad(ModelAndView model) throws IOException {
        Entidad entidad = new Entidad();
        model.addObject("entidad", entidad);
        model.setViewName("entidadForm");
        return model;
    }

    @RequestMapping(value = "/entidades/save", method = RequestMethod.POST)
    public String saveEntidad(@ModelAttribute Entidad entidad, RedirectAttributes redirectAttributes) {
        boolean checkDomicilio = entidadService.checkEntidadByDomicilio(entidad.getDomicilio());
        if (entidad.getId() == 0) {
            if (checkDomicilio) {
                entidadService.addEntidad(entidad);
                redirectAttributes.addFlashAttribute("msg", "<div class='alert alert-success'>La entidad fue creada correctamente.</div>");
            }else{
                redirectAttributes.addFlashAttribute("msg", "<div class='alert alert-danger'>La entidad no pudo ser registrada, ya existe el domicilio <b>"+entidad.getDomicilio()+"</b>.</div>");
            }
        } else {
            Entidad entidadSel = entidadService.getEntidad(entidad.getId());
            if (!checkDomicilio && !entidadSel.getDomicilio().equals(entidad.getDomicilio())) {
                redirectAttributes.addFlashAttribute("msg", "<div class='alert alert-danger'>La entidad no pudo ser registrada, ya existe el domicilio <b>"+entidad.getDomicilio()+"</b>.</div>");
            }else{
                entidadService.updateEntidad(entidad);
                redirectAttributes.addFlashAttribute("msg", "<div class='alert alert-success'>La entidad fue editada correctamente.</div>");
            }
        }            
        return "redirect:/entidades";
    }

    @RequestMapping(value = "/entidades/editar", method = RequestMethod.GET)
    public ModelAndView editEntidad(HttpServletRequest request) {
            int entidadId = Integer.parseInt(request.getParameter("id"));
            Entidad entidad = entidadService.getEntidad(entidadId);
            ModelAndView model = new ModelAndView("entidadForm");
            model.addObject("entidad", entidad);
            return model;
    }

    @RequestMapping(value = "/entidades/eliminar", method = RequestMethod.GET)
    public String deleteEntidad(HttpServletRequest request, RedirectAttributes redirectAttributes) {
            int entidadId = Integer.parseInt(request.getParameter("id"));
            entidadService.deleteEntidad(entidadId);
            redirectAttributes.addFlashAttribute("msg", "<div class='alert alert-success'>La entidad fue eliminada correctamente.</div>");
            return "redirect:/entidades";
    }
}