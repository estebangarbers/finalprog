package com.jwt.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
@Table(name = "sucursales")
public class Sucursal implements Serializable {

	private static final long serialVersionUID = -3465813074586302847L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
        
        @ManyToOne
        @JoinColumn(name = "entidad_id")
        private Entidad entidad;
        
	@Column
	private String domicilio;     
        
        @Transient
        private int empleados;
        
        @OneToMany(mappedBy = "sucursal", cascade = CascadeType.ALL)
        private List<Contrato> contratos = new ArrayList<>();
        
        @OneToMany(mappedBy = "sucursal", cascade = CascadeType.ALL)
        private List<Caso> casos = new ArrayList<>();
        

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
        
        public Entidad getEntidad() {
		return entidad;
	}

	public void setEntidad(Entidad entidad) {
		this.entidad = entidad;
	}

	public String getDomicilio() {
		return domicilio;
	}

	public void setDomicilio(String domicilio) {
		this.domicilio = domicilio;
	}

        /**
         * @return the contratos
         */
        public List<Contrato> getContratos() {
            return contratos;
        }

        /**
         * @param contratos the contratos to set
         */
        public void setContratos(List<Contrato> contratos) {
            this.contratos = contratos;
        }
        
        /**
         * @return the casos
         */
        public List<Caso> getCasos() {
            return casos;
        }

        /**
         * @param casos the casos to set
         */
        public void setCasos(List<Caso> casos) {
            this.casos = casos;
        }
        
        /**
         * @return the empleados
         */
        public int getEmpleados() {
            return empleados;
        }

        /**
         * @param empleados the empleados to set
         */
        public void setEmpleados(int empleados) {
            this.empleados = empleados;
        }
}