package com.jwt.model;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "jueces")
public class Juez implements Serializable {

	private static final long serialVersionUID = -3465813074586302847L;
        
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	@Column
	private String nombre;

	@Column
	private String servicio;    
       
        @OneToMany(mappedBy = "juez", cascade = CascadeType.ALL)
        private List<Caso> casos = new ArrayList<>();
        
        public List<Caso> getCasos() {
            return casos;
        }

        public void setCasos(List<Caso> casos) {
            this.casos = casos;
        }
        
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
        
        public String getServicio() throws ParseException {
            /*SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date d = sdf.parse(servicio);
            sdf.applyPattern("dd/MM/yyyy");
            String newDateString = sdf.format(d);
            return newDateString;*/
            return servicio;
	}

	public void setServicio(String servicio) {
		this.servicio = servicio;
	}

}