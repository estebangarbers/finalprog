package com.jwt.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "contratos")
public class Contrato implements Serializable {

	private static final long serialVersionUID = -3465813074586302847L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
        @Column(name = "id")
	private int id;
        
        @Column(name = "usuario_id")
	private int usuarioId;
        
        @ManyToOne
        @JoinColumn(name = "sucursal_id")
	private Sucursal sucursal;
        
        @Column(name = "fecha")
	private String fecha;
        
        @Column(name = "armado")
	private int armado;

	public int getId() {
            return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUsuarioId() {
		return usuarioId;
	}

	public void setUsuarioId(int usuario_id) {
		this.usuarioId = usuario_id;
	}
        
        public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
            this.fecha = fecha;
	}
        
        public int getArmado() {
            return armado;
	}

	public void setArmado(int armado) {
            this.armado = armado;
	}

        /**
         * @return the sucursal
         */
        public Sucursal getSucursal() {
            return sucursal;
        }

        /**
         * @param sucursal the sucursal to set
         */
        public void setSucursal(Sucursal sucursal) {
            this.sucursal = sucursal;
        }
}