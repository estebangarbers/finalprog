package com.jwt.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "detenidos")
public class Detenido implements Serializable {

	private static final long serialVersionUID = -3465813074586302847L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
        
        @Column
	private String nombre;

	@Column
	private String banda;
        
        @Transient
	private int miembros;

        public int getMiembros() {
            return miembros;
        }

        public void setMiembros(int miembros) {
            this.miembros = miembros;
        }
        
        @OneToMany(mappedBy = "detenido", cascade = CascadeType.ALL)
        private List<Caso> casos = new ArrayList<>();

        public List<Caso> getCasos() {
            return casos;
        }

        public void setCasos(List<Caso> casos) {
            this.casos = casos;
        }       

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
        
        public String getNombre() {
            return nombre;
	}

	public void setNombre(String nombre) {
            this.nombre = nombre;
	}

	public String getBanda() {
            return banda;
	}

	public void setBanda(String banda) {
            this.banda = banda;
	}
}