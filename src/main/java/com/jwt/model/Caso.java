package com.jwt.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "casos")
public class Caso implements Serializable {

	private static final long serialVersionUID = -3465813074586302847L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
        @Column(name = "id")
	private int id;
        
        @Column(name = "fecha")
	private String fecha;
               
        @ManyToOne
        @JoinColumn(name = "sucursal_id")
        private Sucursal sucursal;
        
        @ManyToOne
        @JoinColumn(name = "detenido_id")
        private Detenido detenido;
        
        @ManyToOne
        @JoinColumn(name = "juez_id")
        private Juez juez;
        
        @Column(name = "condena")
	private int condena;
        
        public Juez getJuez() {
            return juez;
        }

        public void setJuez(Juez juez) {
            this.juez = juez;
        }

        public Detenido getDetenido() {
            return detenido;
        }

        public void setDetenido(Detenido detenido) {
            this.detenido = detenido;
        }

	public int getId() {
            return id;
	}

	public void setId(int id) {
		this.id = id;
	}
        
        public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
            this.fecha = fecha;
	}
        
        public int getCondena() {
            return condena;
	}

	public void setCondena(int condena) {
            this.condena = condena;
	}
        
        public Sucursal getSucursal() {
            return sucursal;
        }

        public void setSucursal(Sucursal sucursal) {
            this.sucursal = sucursal;
        }
}