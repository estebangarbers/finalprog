package com.jwt.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javax.persistence.CascadeType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "entidades")
public class Entidad implements Serializable {

	private static final long serialVersionUID = -3465813074586302847L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public int id;

	@Column
	private String domicilio;
        
        @OneToMany(mappedBy = "entidad", cascade = CascadeType.ALL)
        private List<Sucursal> sucursales = new ArrayList<>();

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDomicilio() {
		return domicilio;
	}

	public void setDomicilio(String domicilio) {
		this.domicilio = domicilio;
	}

        /**
         * @return the sucursales
         */
        public List<Sucursal> getSucursales() {
            return sucursales;
        }

        /**
         * @param sucursales the sucursales to set
         */
        public void setSucursales(List<Sucursal> sucursales) {
            this.sucursales = sucursales;
        }

}