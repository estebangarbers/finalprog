<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Casos | Ministerio del Interior</title>
<link rel='shortcut icon' type='image/x-icon' href='${pageContext.request.contextPath}/resources/img/favicon.ico' />
<link href="${pageContext.request.contextPath}/resources/css/site.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
<script src="${pageContext.request.contextPath}/resources/js/site.min.js"></script>
</head>
<body>
    <jsp:include page="header.jsp"/>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h3>Casos</h3>
            </div>
            <div class="col-md-6">
                <c:if test="${sessionScope.userRol.equals('Administrador')}">   
                <a href="casos/nuevo" style="margin-top: 15px;" class="btn btn-success pull-right">Nuevo caso</a>
                </c:if>
            </div>
        </div>                
        <hr>
        ${msg}
        <table class="table table-bordered">
            <th width="1%">ID</th>
            <th>Fecha</th>
            <th>Detenido</th>
            <th>Sucursal</th>
            <th>Juez a cargo</th>
            <th>Condena</th>
            <c:if test="${sessionScope.userRol.equals('Administrador')}">   
            <th width="100px"></th>
            </c:if>
            <c:forEach var="caso" items="${listCasos}">
            <tr>
                <td>${caso.id}</td>
                <td>${caso.fecha}</td>
                <td>${caso.detenido.nombre} <br><span class="text-muted" style="font-size:0.8em;"><b>Banda:</b> ${caso.detenido.banda}</span></td>
                <td>${caso.sucursal.id} - ${caso.sucursal.domicilio} <br><span class="text-muted" style="font-size:0.8em;"><b>Entidad:</b> ${caso.sucursal.entidad.domicilio}</span></td>
                <td>${caso.juez.nombre}</td>
                <td>
                    <c:if test = "${caso.condena == 0}">
                        Sin condena
                    </c:if>
                    <c:if test = "${caso.condena > 0}">
                        ${caso.condena} años
                    </c:if>
                </td>
                <c:if test="${sessionScope.userRol.equals('Administrador')}">   
                <td class="text-center">
                    <div class="btn-group">
                    <a href="casos/editar?id=${caso.id}" class="btn btn-warning"><i class="fa fa-edit"></i></a>
                    <a href="casos/eliminar?id=${caso.id}" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                    </div>
                </td>
                </c:if>
            </tr>
            </c:forEach>
        </table>
    </div>
</body>
</html>