<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Iniciar sesión | Ministerio del Interior</title>
<link rel='shortcut icon' type='image/x-icon' href='${pageContext.request.contextPath}/resources/img/favicon.ico' />
<link href="${pageContext.request.contextPath}/resources/css/site.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
<style>
    body{
        padding:40px 0;
    }
</style>
</head>
<body>
    <div class="container">
        <div class="col-md-4 col-md-offset-4">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title">Iniciar sesión</h3>
              </div>
              <div class="panel-body">
                <form action="checkAuth" method="POST">
                <div class="col-md-12">
                    ${msg}
                </div>
                <div class="col-md-12">
                <div class="form-group">
                    <label>Nombre de usuario:</label>
                    <input type="text" class="form-control" name="user" placeholder="Ingrese aquí su usuario" autcompleto="off" required>
                </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Contraseña:</label>
                        <input type="password" class="form-control" name="password" placeholder="Ingrese aquí su conraseña" autocomplete="off" required>
                    </div>
                </div>
                <div class="col-md-12">
                    <button type="submit" class="btn btn-primary pull-right">Iniciar sesión</button>
                </div>
                </form>
              </div>
            </div>
        </div>
    </div>
</body>
</html>