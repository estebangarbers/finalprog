<style>
    .table tr td{
        vertical-align:middle!important;
    }
</style>
<nav class="navbar navbar-inverse" role="navigation" style="border-radius:0;">
                  <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-5">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                      </button>
                      <a class="navbar-brand" href="${pageContext.request.contextPath}/home">Ministerio del Interior</a>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-5">
                      <ul class="nav navbar-nav">
                        <li><a href="${pageContext.request.contextPath}/home">Inicio</a></li>
                        <!-- <li class="disabled"><a href="#">Link</a></li> -->
                        <li class="dropdown">
                          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Sistema <b class="caret"></b></a>
                          <ul class="dropdown-menu" role="menu">
                            <li><a href="${pageContext.request.contextPath}/entidades">Entidades</a></li>
                            <li><a href="${pageContext.request.contextPath}/sucursales">Sucursales</a></li>
                            <li class="divider"></li>                            
                            <li><a href="${pageContext.request.contextPath}/vigilantes">Vigilantes</a></li>
                            <li class="divider"></li>
                            <li><a href="${pageContext.request.contextPath}/detenidos">Detenidos</a></li>
                            <li class="divider"></li>
                            <li><a href="${pageContext.request.contextPath}/casos">Casos</a></li>
                            <li><a href="${pageContext.request.contextPath}/jueces">Jueces</a></li>
                            <li><a href="${pageContext.request.contextPath}/investigadores">Investigadores</a></li>
                          </ul>
                        </li>
                      </ul>
                        <p class="navbar-text navbar-right"><a class="navbar-link" href="${pageContext.request.contextPath}/logout">Sesi�n iniciada: <%= session.getAttribute("userName") %> | Cerrar sesi�n</a></p>
                    </div><!-- /.navbar-collapse -->
                  </div><!-- /.container-fluid -->
                </nav>