$(function() {
    if ($('#banda').val() != '0') {
        $('#nombreBandaCont').hide(); 
        $('#nombreBanda').prop( "disabled", true);
    }
    $('#banda').change(function(){
        if($('#banda').val() === '0') {
            $('#nombreBandaCont').show(); 
            $('#nombreBanda').prop( "disabled", false);
        } else {
            $('#nombreBandaCont').hide(); 
            $('#nombreBanda').prop( "disabled", true);
        } 
    });
});

$(function() {
    if ($('#hubocondena').val() != '1') {
        $('#condenaCont').hide(); 
        $('#condena').prop( "disabled", true);
    }
    $('#hubocondena').change(function(){
        if($('#hubocondena').val() === '1') {
            $('#condenaCont').show(); 
            $('#condena').prop( "disabled", false);
        } else {
            $('#condenaCont').hide(); 
            $('#condena').prop( "disabled", true);
        } 
    });
});